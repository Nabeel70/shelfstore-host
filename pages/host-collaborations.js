import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./host-collaborations.module.css";

const HostCollaborations = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onButtonClick = useCallback(() => {
    router.push("/host-collaboration");
  }, [router]);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  const onButton1Click = useCallback(() => {
    router.push("/host-collaborations-edit");
  }, [router]);


  return (
    <>
      <div className={styles.hostCollaborationsDiv}>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <h1 className={styles.collaborationsH1}>Collaborations</h1>
        <div className={styles.frameDiv}>
          <Link href="/host-account">
            <a className={styles.profileA}>Profile</a>
          </Link>
          <a className={styles.collaborations} href="/">
            Collaborations
          </a>
          <Link href="/my-spaces">
            <a className={styles.mySpacesForRent}>My Spaces for Rent</a>
          </Link>
          <Link href="/host-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <Link href="/host-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <Link href="/host-payments">
            <a className={styles.payments}>Payments</a>
          </Link>
        </div>
        <div className={styles.section2Div}>
          <div className={styles.lineDiv2} />
          <h1 className={styles.receivedRequestsH1}>Received Requests</h1>
          <div className={styles.frameDiv1}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-15@2x.png"
            />
            <h6 className={styles.wallSpaceLeftOfEntrance}>
              wall space left of entrance
            </h6>
            <p className={styles.theBullP}>The Bull</p>
            <p className={styles.p}>21/05/2022 - 23/05/2022</p>
            <button className={styles.button} onClick={onButtonClick}>
              <div className={styles.labelTextDiv}>View Request</div>
            </button>
          </div>
        </div>
        <footer className={styles.groupFooter}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv3} />
        </footer>
        <div className={styles.section1Div}>
          <div className={styles.lineDiv2} />
          <h1 className={styles.confirmedCollaborationsH1}>
            Confirmed Collaborations
          </h1>
          <div className={styles.frameDiv2}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-154@2x.png"
            />
            <h6 className={styles.wallSpaceLeftOfEntrance}>Alpha Costumes</h6>
            <p className={styles.theBullP}>Shelf Space</p>
            <p className={styles.p1}>08/04/2022 - 08/05/2022</p>
            <button className={styles.button} onClick={onButton1Click}>
              <div className={styles.labelTextDiv}>Cancel Collaboration</div>
            </button>
          </div>
          <p
            className={styles.makeSureToPrintOffYourIn}
          >{`Make sure to print off your info sign and place it next your products in the space, as this contains information about you and the code needed for shoppers to purchase your items. You can generate this sign at any time by clicking on “Your Products Page” and then clicking on “Generate Info and Payment Printout”. `}</p>
          <p
            className={styles.clickOnYourProductsPage}
          >{`Click on “Your Products Page” to add products that will be on display. `}</p>
          <img className={styles.errorIcon} alt="" src="../error2.svg" />
          <img className={styles.errorIcon1} alt="" src="../error3.svg" />
        </div>
        <div className={styles.lineDiv5} />
        <div className={styles.section3Div}>
          <h1 className={styles.sentNudgesH1}>Sent Nudges</h1>
          <p
            className={styles.sendANudgeToVendorsAsAS}
          >{`Send a nudge to vendors as a simple way  to say that you would love to work withthem! You can nudge them by visiting their profile page. `}</p>
          <h6 className={styles.orlyArtistH6}>Orly Artist</h6>
          <p className={styles.p2}>28/04/2022</p>
          <h6 className={styles.madHouseMerch}>Mad House Merch</h6>
          <p className={styles.p3}>21/03/2022</p>
          <h6 className={styles.potlyfeH6}>Potlyfe</h6>
          <p className={styles.p4}>{`14/02/2022 `}</p>
        </div>
        <div className={styles.frameDiv3}>
          <div className={styles.frameDiv4}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv2}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <div className={styles.hostAccountDiv}>Host Account</div>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default HostCollaborations;
