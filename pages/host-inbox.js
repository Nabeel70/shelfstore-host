import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./host-inbox.module.css";

const HostInbox = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  const onCloseButtonClick = useCallback(() => {
    router.push("/host-messages");
  }, [router]);

  return (
    <>
      <div className={styles.hostInboxDiv}>
        <Link href="/host-account">
          <a className={styles.profileA}>Profile</a>
        </Link>
        <Link href="/host-collaborations">
          <a className={styles.collaborations}>Collaborations</a>
        </Link>
        <Link href="/my-spaces">
          <a className={styles.mySpacesForRent}>My Spaces for Rent</a>
        </Link>
        <Link href="/host-account-setting">
          <a className={styles.accountSettings}>Account Settings</a>
        </Link>
        <Link href="/host-messages">
          <a className={styles.messages}>Messages</a>
        </Link>
        <Link href="/host-payments">
          <a className={styles.payments}>Payments</a>
        </Link>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <footer className={styles.groupFooter}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv2} />
        </footer>
        <div className={styles.frameDiv}>
          <div className={styles.frameDiv1}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <div className={styles.hostAccountDiv}>Host Account</div>
        </div>
        <a className={styles.inboxA} href="/">
          Inbox
        </a>
        <a className={styles.sentA} href="/">
          Sent
        </a>
        <div className={styles.lineDiv3} />
        <div className={styles.lineDiv4} />
        <h1 className={styles.messagesH1}>Messages</h1>
        <div className={styles.messageDetailDiv}>
          <h6 className={styles.subjectOpenSpaceOnCounter}>
            Subject: Open Space on Counter
          </h6>
          <h6 className={styles.fromAlphaCostumes}>From: Alpha Costumes</h6>
          <h6 className={styles.sent070420221235}>Sent: 07/04/2022 12:35</h6>
          <div className={styles.whatTimeWouldYouLikeMeTo}>
            <p className={styles.whatTimeWould}>
              What time would you like me to come round?
            </p>
            <p className={styles.whatTimeWould}>&nbsp;</p>
            <p className={styles.whatTimeWould}>Best,</p>
            <p className={styles.alphacostumes}>Alphacostumes</p>
          </div>
          <button className={styles.closeButton} onClick={onCloseButtonClick}>
            <img className={styles.closeIcon} alt="" src="../close.svg" />
          </button>
          <button className={styles.replyButton}>
            <img className={styles.replyIcon} alt="" src="../reply.svg" />
          </button>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default HostInbox;
