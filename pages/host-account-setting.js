import { useState, useCallback } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form } from "react-bootstrap";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./host-account-setting.module.css";

const HostAccountSetting = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <div className={styles.hostAccountSettingDiv}>
        <h1 className={styles.accountSertingsH1}>Account Sertings</h1>
        <div className={styles.sidebarDiv}>
          <Link href="/host-account">
            <a className={styles.profileA}>Profile</a>
          </Link>
          <Link href="/host-collaborations">
            <a className={styles.collaborations}>Collaborations</a>
          </Link>
          <Link href="/my-spaces">
            <a className={styles.mySpacesForRent}>My Spaces for Rent</a>
          </Link>
          <a className={styles.accountSettings} href="/">
            Account Settings
          </a>
          <Link href="/host-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <Link href="/host-payments">
            <a className={styles.paymentSettings}>Payment Settings</a>
          </Link>
        </div>
        <div className={styles.section4Div}>
          <div className={styles.lineDiv} />
          <h2 className={styles.securityH2}>Security</h2>
          <p
            className={styles.deactivateYourAccount}
          >{`Deactivate your account. `}</p>
          <Link href="/host-account">
            <a className={styles.deactivate}>Deactivate</a>
          </Link>
        </div>
        <div className={styles.section3Div}>
          <p className={styles.messagesP}>Messages</p>
          <div className={styles.lineDiv} />
          <h2 className={styles.securityH2}>Email Notifications</h2>
          <p className={styles.updatesFromRentmyshelf}>
            Updates from rentmyshelf
          </p>
          <p className={styles.requestsFromVendors}>Requests from Vendors</p>
        </div>
        <div className={styles.section2Div}>
          <div className={styles.lineDiv} />
          <h2 className={styles.securityH2}>Login</h2>
          <h2 className={styles.passwordH2}>Password.</h2>
          <a className={styles.update} href="/">
            Update
          </a>
        </div>
        <div className={styles.section1Div}>
          <div className={styles.lineDiv} />
          <h2 className={styles.securityH2}>Contact Details</h2>
          <h6 className={styles.emailH6}>Email:</h6>
          <Link href="/host-account">
            <a className={styles.update1}>Update</a>
          </Link>
          <a
            className={styles.hughebdyhotmailcom}
            href="mailto:hughebdy@hotmail.com"
          >
            hughebdy@hotmail.com
          </a>
          <p className={styles.contactNumberP}>Contact Number:</p>
          <Link href="/host-account">
            <a className={styles.update2}>Update</a>
          </Link>
          <a className={styles.a} href="tel:07840239745">
            07840239745
          </a>
        </div>
        <Form.Check className={styles.checkBoxFormCheck} isInvalid />
        <Form.Check className={styles.checkBoxFormCheck1} isInvalid />
        <Form.Check
          className={styles.checkBoxOutlineBlankFormCheck}
          isInvalid
        />
        <div className={styles.lineDiv4} />
        <footer className={styles.footer}>
          <div className={styles.rentmyshelfLtdDiv}>
            © 2022 rentmyshelf, ltd.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv5} />
        </footer>
        <div className={styles.lineDiv6} />
        <div className={styles.headerDiv}>
          <div className={styles.frameDiv}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <div className={styles.hostAccountDiv}>Host Account</div>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default HostAccountSetting;
