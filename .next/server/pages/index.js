/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./pages/index.module.css":
/*!********************************!*\
  !*** ./pages/index.module.css ***!
  \********************************/
/***/ ((module) => {

eval("// Exports\nmodule.exports = {\n\t\"rectangleDiv\": \"index_rectangleDiv__KcQhf\",\n\t\"shareourshelfH1\": \"index_shareourshelfH1__34dJW\",\n\t\"theCollaborativeMarketplace\": \"index_theCollaborativeMarketplace__sQuzz\",\n\t\"shareSpan\": \"index_shareSpan__nGiz7\",\n\t\"aSpan\": \"index_aSpan__2fqWg\",\n\t\"shelfSpan\": \"index_shelfSpan__SXIt_\",\n\t\"shareourshelfTxtSpan\": \"index_shareourshelfTxtSpan__gweaP\",\n\t\"shareourshelfH11\": \"index_shareourshelfH11__M4h4O\",\n\t\"startMain\": \"index_startMain__m9sGy\"\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC5tb2R1bGUuY3NzLmpzIiwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZXMiOlsid2VicGFjazovL3NoYXJhc2hlbGYvLi9wYWdlcy9pbmRleC5tb2R1bGUuY3NzP2Q4NTkiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gRXhwb3J0c1xubW9kdWxlLmV4cG9ydHMgPSB7XG5cdFwicmVjdGFuZ2xlRGl2XCI6IFwiaW5kZXhfcmVjdGFuZ2xlRGl2X19LY1FoZlwiLFxuXHRcInNoYXJlb3Vyc2hlbGZIMVwiOiBcImluZGV4X3NoYXJlb3Vyc2hlbGZIMV9fMzRkSldcIixcblx0XCJ0aGVDb2xsYWJvcmF0aXZlTWFya2V0cGxhY2VcIjogXCJpbmRleF90aGVDb2xsYWJvcmF0aXZlTWFya2V0cGxhY2VfX3NRdXp6XCIsXG5cdFwic2hhcmVTcGFuXCI6IFwiaW5kZXhfc2hhcmVTcGFuX19uR2l6N1wiLFxuXHRcImFTcGFuXCI6IFwiaW5kZXhfYVNwYW5fXzJmcVdnXCIsXG5cdFwic2hlbGZTcGFuXCI6IFwiaW5kZXhfc2hlbGZTcGFuX19TWEl0X1wiLFxuXHRcInNoYXJlb3Vyc2hlbGZUeHRTcGFuXCI6IFwiaW5kZXhfc2hhcmVvdXJzaGVsZlR4dFNwYW5fX2d3ZWFQXCIsXG5cdFwic2hhcmVvdXJzaGVsZkgxMVwiOiBcImluZGV4X3NoYXJlb3Vyc2hlbGZIMTFfX000aDRPXCIsXG5cdFwic3RhcnRNYWluXCI6IFwiaW5kZXhfc3RhcnRNYWluX19tOXNHeVwiXG59O1xuIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/index.module.css\n");

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _index_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.module.css */ \"./pages/index.module.css\");\n/* harmony import */ var _index_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_index_module_css__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\nconst Start = ()=>{\n    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();\n    const onRectangleRectangleClick = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)(()=>{\n        router.push(\"/\");\n    }, [\n        router\n    ]);\n    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{\n        window.setTimeout(function() {\n            window.location.href = \"/home\";\n        }, 1000);\n    }), /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"main\", {\n        className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().startMain),\n        id: \"main\",\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().rectangleDiv),\n                onClick: onRectangleRectangleClick\n            }, void 0, false, {\n                fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                lineNumber: 19,\n                columnNumber: 7\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().shareourshelfH1),\n                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n                    className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().shareourshelfH11),\n                    children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"span\", {\n                        className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().shareourshelfTxtSpan),\n                        children: [\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"span\", {\n                                className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().shareSpan),\n                                children: \"share\"\n                            }, void 0, false, {\n                                fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                                lineNumber: 26,\n                                columnNumber: 13\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"span\", {\n                                className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().aSpan),\n                                children: \"A\"\n                            }, void 0, false, {\n                                fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                                lineNumber: 27,\n                                columnNumber: 13\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"span\", {\n                                className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().shelfSpan),\n                                children: \"shelf\"\n                            }, void 0, false, {\n                                fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                                lineNumber: 28,\n                                columnNumber: 13\n                            }, undefined)\n                        ]\n                    }, void 0, true, {\n                        fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                        lineNumber: 25,\n                        columnNumber: 11\n                    }, undefined)\n                }, void 0, false, {\n                    fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                    lineNumber: 24,\n                    columnNumber: 9\n                }, undefined)\n            }, void 0, false, {\n                fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                lineNumber: 23,\n                columnNumber: 7\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h5\", {\n                className: (_index_module_css__WEBPACK_IMPORTED_MODULE_3___default().theCollaborativeMarketplace),\n                children: \"The Collaborative Marketplace\"\n            }, void 0, false, {\n                fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n                lineNumber: 32,\n                columnNumber: 7\n            }, undefined)\n        ]\n    }, void 0, true, {\n        fileName: \"H:\\\\shareAshelf\\\\pages\\\\index.js\",\n        lineNumber: 18,\n        columnNumber: 5\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Start);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQTtBQUFxRDtBQUNiO0FBQ0E7QUFFeEMsTUFBTUssS0FBSyxHQUFHLElBQU07SUFDbEIsTUFBTUMsTUFBTSxHQUFHSCxzREFBUyxFQUFFO0lBRTFCLE1BQU1JLHlCQUF5QixHQUFHTCxrREFBVyxDQUFDLElBQU07UUFDbERJLE1BQU0sQ0FBQ0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25CLENBQUMsRUFBRTtRQUFDRixNQUFNO0tBQUMsQ0FBQztJQUVaLE9BQ0VMLGdEQUFTLENBQUMsSUFBTTtRQUNkUSxNQUFNLENBQUNDLFVBQVUsQ0FBQyxXQUFXO1lBQzNCRCxNQUFNLENBQUNFLFFBQVEsQ0FBQ0MsSUFBSSxHQUFHLE9BQU8sQ0FBQztRQUNuQyxDQUFDLEVBQUUsSUFBSSxDQUFDO0lBQ1YsQ0FBQyxDQUFDLGdCQUNBLDhEQUFDQyxNQUFJO1FBQUNDLFNBQVMsRUFBRVYsb0VBQWdCO1FBQUVZLEVBQUUsRUFBQyxNQUFNOzswQkFDMUMsOERBQUNDLEtBQUc7Z0JBQ0ZILFNBQVMsRUFBRVYsdUVBQW1CO2dCQUM5QmUsT0FBTyxFQUFFWix5QkFBeUI7Ozs7O3lCQUNsQzswQkFDRiw4REFBQ1UsS0FBRztnQkFBQ0gsU0FBUyxFQUFFViwwRUFBc0I7MEJBQ3BDLDRFQUFDaUIsSUFBRTtvQkFBQ1AsU0FBUyxFQUFFViwyRUFBdUI7OEJBQ3BDLDRFQUFDbUIsTUFBSTt3QkFBQ1QsU0FBUyxFQUFFViwrRUFBMkI7OzBDQUMxQyw4REFBQ21CLE1BQUk7Z0NBQUNULFNBQVMsRUFBRVYsb0VBQWdCOzBDQUFFLE9BQUs7Ozs7O3lDQUFPOzBDQUMvQyw4REFBQ21CLE1BQUk7Z0NBQUNULFNBQVMsRUFBRVYsZ0VBQVk7MENBQUUsR0FBQzs7Ozs7eUNBQU87MENBQ3ZDLDhEQUFDbUIsTUFBSTtnQ0FBQ1QsU0FBUyxFQUFFVixvRUFBZ0I7MENBQUUsT0FBSzs7Ozs7eUNBQU87Ozs7OztpQ0FDMUM7Ozs7OzZCQUNKOzs7Ozt5QkFDRDswQkFDTiw4REFBQ3dCLElBQUU7Z0JBQUNkLFNBQVMsRUFBRVYsc0ZBQWtDOzBCQUFFLCtCQUVuRDs7Ozs7eUJBQUs7Ozs7OztpQkFDQSxDQUNQO0FBQ0osQ0FBQztBQUVELGlFQUFlQyxLQUFLLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zaGFyYXNoZWxmLy4vcGFnZXMvaW5kZXguanM/YmVlNyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VDYWxsYmFja30gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgc3R5bGVzIGZyb20gXCIuL2luZGV4Lm1vZHVsZS5jc3NcIjtcclxuXHJcbmNvbnN0IFN0YXJ0ID0gKCkgPT4ge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG5cclxuICBjb25zdCBvblJlY3RhbmdsZVJlY3RhbmdsZUNsaWNrID0gdXNlQ2FsbGJhY2soKCkgPT4ge1xyXG4gICAgcm91dGVyLnB1c2goXCIvXCIpO1xyXG4gIH0sIFtyb3V0ZXJdKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAgIHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9ob21lJztcclxuICAgIH0sIDEwMDApXHJcbiAgfSksXHJcbiAgICA8bWFpbiBjbGFzc05hbWU9e3N0eWxlcy5zdGFydE1haW59IGlkPVwibWFpblwiPlxyXG4gICAgICA8ZGl2XHJcbiAgICAgICAgY2xhc3NOYW1lPXtzdHlsZXMucmVjdGFuZ2xlRGl2fVxyXG4gICAgICAgIG9uQ2xpY2s9e29uUmVjdGFuZ2xlUmVjdGFuZ2xlQ2xpY2t9XHJcbiAgICAgIC8+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuc2hhcmVvdXJzaGVsZkgxfT5cclxuICAgICAgICA8aDEgY2xhc3NOYW1lPXtzdHlsZXMuc2hhcmVvdXJzaGVsZkgxMX0+XHJcbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3N0eWxlcy5zaGFyZW91cnNoZWxmVHh0U3Bhbn0+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17c3R5bGVzLnNoYXJlU3Bhbn0+c2hhcmU8L3NwYW4+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17c3R5bGVzLmFTcGFufT5BPC9zcGFuPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3N0eWxlcy5zaGVsZlNwYW59PnNoZWxmPC9zcGFuPlxyXG4gICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgIDwvaDE+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8aDUgY2xhc3NOYW1lPXtzdHlsZXMudGhlQ29sbGFib3JhdGl2ZU1hcmtldHBsYWNlfT5cclxuICAgICAgICBUaGUgQ29sbGFib3JhdGl2ZSBNYXJrZXRwbGFjZVxyXG4gICAgICA8L2g1PlxyXG4gICAgPC9tYWluPlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBTdGFydDtcclxuIl0sIm5hbWVzIjpbIlJlYWN0IiwidXNlRWZmZWN0IiwidXNlQ2FsbGJhY2siLCJ1c2VSb3V0ZXIiLCJzdHlsZXMiLCJTdGFydCIsInJvdXRlciIsIm9uUmVjdGFuZ2xlUmVjdGFuZ2xlQ2xpY2siLCJwdXNoIiwid2luZG93Iiwic2V0VGltZW91dCIsImxvY2F0aW9uIiwiaHJlZiIsIm1haW4iLCJjbGFzc05hbWUiLCJzdGFydE1haW4iLCJpZCIsImRpdiIsInJlY3RhbmdsZURpdiIsIm9uQ2xpY2siLCJzaGFyZW91cnNoZWxmSDEiLCJoMSIsInNoYXJlb3Vyc2hlbGZIMTEiLCJzcGFuIiwic2hhcmVvdXJzaGVsZlR4dFNwYW4iLCJzaGFyZVNwYW4iLCJhU3BhbiIsInNoZWxmU3BhbiIsImg1IiwidGhlQ29sbGFib3JhdGl2ZU1hcmtldHBsYWNlIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/index.js\n");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.js"));
module.exports = __webpack_exports__;

})();