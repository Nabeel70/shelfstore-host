import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./host-collaborations-edit.module.css";

const HostCollaborationsEdit = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onButtonClick = useCallback(() => {
    router.push("/host-collaboration");
  }, [router]);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  const onButton1Click = useCallback(() => {
    router.push("/host-collaborations");
  }, [router]);


  return (
    <>
      <div className={styles.hostCollaborationsEditDiv}>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <h1 className={styles.collaborationsH1}>Collaborations</h1>
        <div className={styles.frameDiv}>
          <Link href="/host-account">
            <a className={styles.profileA}>Profile</a>
          </Link>
          <Link href="/host-collaborations">
            <a className={styles.collaborations}>Collaborations</a>
          </Link>
          <Link href="/my-spaces">
            <a className={styles.mySpacesForRent}>My Spaces for Rent</a>
          </Link>
          <Link href="/host-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <Link href="/host-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <Link href="/host-payments">
            <a className={styles.payments}>Payments</a>
          </Link>
        </div>
        <div className={styles.section2Div}>
          <div className={styles.lineDiv2} />
          <h1 className={styles.receivedRequestsH1}>Received Requests</h1>
          <div className={styles.frameDiv1}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-15@2x.png"
            />
            <h6 className={styles.wallSpaceLeftOfEntrance}>
              wall space left of entrance
            </h6>
            <p className={styles.theBullP}>The Bull</p>
            <p className={styles.p}>21/05/2022 - 23/05/2022</p>
            <button className={styles.button} onClick={onButtonClick}>
              <div className={styles.labelTextDiv}>View Request</div>
            </button>
          </div>
        </div>
        <footer className={styles.groupFooter}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv3} />
        </footer>
        <div className={styles.section1Div}>
          <div className={styles.lineDiv2} />
          <h1 className={styles.confirmedCollaborationsH1}>
            Confirmed Collaborations
          </h1>
          <p
            className={styles.makeSureToPrintOffYourIn}
          >{`Make sure to print off your info sign and place it next your products in the space, as this contains information about you and the code needed for shoppers to purchase your items. You can generate this sign at any time by clicking on “Your Products Page” and then clicking on “Generate Info and Payment Printout”. `}</p>
          <p
            className={styles.clickOnYourProductsPage}
          >{`Click on “Your Products Page” to add products that will be on display. `}</p>
          <img className={styles.errorIcon} alt="" src="../error.svg" />
          <img className={styles.errorIcon1} alt="" src="../error1.svg" />
        </div>
        <div className={styles.lineDiv5} />
        <div className={styles.section3Div}>
          <h1 className={styles.sentNudgesH1}>Sent Nudges</h1>
          <p
            className={styles.sendANudgeToVendorsAsAS}
          >{`Send a nudge to vendors as a simple way  to say that you would love to work withthem! You can nudge them by visiting their profile page. `}</p>
          <h6 className={styles.orlyArtistH6}>Orly Artist</h6>
          <p className={styles.p1}>28/04/2022</p>
          <h6 className={styles.madHouseMerch}>Mad House Merch</h6>
          <p className={styles.p2}>21/03/2022</p>
          <h6 className={styles.potlyfeH6}>Potlyfe</h6>
          <p className={styles.p3}>{`14/02/2022 `}</p>
        </div>
        <div className={styles.frameDiv2}>
          <div className={styles.frameDiv3}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv1}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <div className={styles.hostAccountDiv}>Host Account</div>
        </div>
        <div className={styles.frameDiv4}>
          <img
            className={styles.rectangleIcon}
            alt=""
            src="../rectangle-151@2x.png"
          />
          <b className={styles.shelfSpaceB}>Shelf Space</b>
          <div className={styles.jamesCafeDiv}>James’ Cafe</div>
          <div className={styles.div}>08/04/2022 - 08/05/2022</div>
          <div className={styles.buttonDiv}>
            <div className={styles.labelTextDiv2}>Cancel Collaboration</div>
          </div>
          <div className={styles.frameDiv5}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-151@2x.png"
            />
            <b className={styles.shelfSpaceB}>Shelf Space</b>
            <div className={styles.jamesCafeDiv}>James’ Cafe</div>
            <div className={styles.div}>08/04/2022 - 08/05/2022</div>
            <div className={styles.buttonDiv1}>
              <div className={styles.labelTextDiv3}>Your Products Page</div>
            </div>
            <div className={styles.buttonDiv2}>
              <div className={styles.labelTextDiv3}>Cancel Collaboration</div>
            </div>
            <div className={styles.frameDiv6}>
              <button className={styles.button1} onClick={onButton1Click}>
                <div className={styles.labelTextDiv5}>Cancel Collaboration</div>
              </button>
              <h2 className={styles.confirmCancellationH2}>
                Confirm Cancellation?
              </h2>
              <Link href="/host-collaborations">
                <a className={styles.closeA}>
                  <img
                    className={styles.closeIcon}
                    alt=""
                    src="../close1.svg"
                  />
                </a>
              </Link>
            </div>
            <h2 className={styles.shelfSpaceH2}>Shelf Space</h2>
            <p className={styles.jamesCafeP}>James’ Cafe</p>
            <p className={styles.p4}>08/04/2022 - 08/05/2022</p>
            <p className={styles.cannotBeUndoneBeSureToC}>
              Cannot be undone. Be sure to communicate intentions with your host
              first.
            </p>
          </div>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default HostCollaborationsEdit;
