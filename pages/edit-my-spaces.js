import { useEffect, useState, useCallback } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { TextField, Icon } from "@mui/material";
import { LocalizationProvider, DatePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { Form } from "react-bootstrap";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./edit-my-spaces.module.css";

const EditMySpaces = () => {
  const [datePickerDateTimePickerValue, setDatePickerDateTimePickerValue] =
    useState(null);
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (

    useEffect(() => {

    

const cover_input = document.querySelector('#cover_input');
var uploaded_image = "";
cover_input.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-photo").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
}); 

const pic_input = document.querySelector('#pic_input');
var uploaded_image = "";
pic_input.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-pic").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
}); 

const img_input = document.querySelector('#img_input');
var uploaded_image = "";
img_input.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-img").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
}); 

}),
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <>
        <div className={styles.editMySpacesDiv}>
          <h1 className={styles.currentSpacesAvailable}>
            Current Spaces Available:
          </h1>
          <div className={styles.lineDiv} />
          <h1 className={styles.mySpacesForRent}>My Spaces for Rent</h1>
          <div className={styles.sidebarDiv}>
            <Link href="/host-account">
              <a className={styles.profileA}>Profile</a>
            </Link>
            <Link href="/host-collaborations">
              <a className={styles.collaborations}>Collaborations</a>
            </Link>
            <Link href="/my-spaces">
              <a className={styles.mySpacesForRent1}>My Spaces for Rent</a>
            </Link>
            <Link href="/host-account-setting">
              <a className={styles.accountSettings}>Account Settings</a>
            </Link>
            <Link href="/host-messages">
              <a className={styles.messages}>Messages</a>
            </Link>
            <Link href="/host-payments">
              <a className={styles.payments}>Payments</a>
            </Link>
          </div>
          <div className={styles.frameDiv}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-151@2x.png"
            />
            <h6 className={styles.shelfSpaceH6}>Shelf Space</h6>
            <a className={styles.buttonA} href="/">
              <div className={styles.labelTextDiv}>Edit</div>
            </a>
          </div>
          <div id="cover-img" className={styles.frameDiv1}>
          
             <input className={styles.addInput} type="file" id="img_input" hidden/>
        <label className={styles.addInput} for="img_input">+</label>

          </div>
          <div className={styles.lineDiv1} />
          <div className={styles.lineDiv2} />
          <footer className={styles.groupFooter}>
            <div className={styles.rentmyshelfIncDiv}>
              © 2022 rentmyshelf, Inc.
            </div>
            <div className={styles.supportDiv}>Support</div>
            <div className={styles.contactDiv}>Contact</div>
            <div className={styles.termsOfService}>Terms of Service</div>
            <div className={styles.lineDiv3} />
          </footer>
          <div className={styles.lineDiv4} />
          <div className={styles.spaceFormDiv}>
            <div id="cover-pic" className={styles.frameDiv2}>

                    <input className={styles.addInput1} type="file" id="pic_input" hidden/>
        <label className={styles.addInput1} for="pic_input">+</label>

            </div>
            <div id="cover-photo" className={styles.frameDiv3}>

                    <input className={styles.buttonInput} type="file" id="cover_input" hidden/>
        <label className={styles.buttonInput} for="cover_input">Change Photo</label>
            </div>
            <h1 className={styles.newSpaceH1}>New Space</h1>
            <form action="/host-account" method="post" autoComplete="on">
            <p
              className={styles.shortDescriptionEgLeft}
            >{`Short description - e.g. left of main entrance, three shelves, each 30 x 100 cm `}</p>
            <p className={styles.photosAddUpToThreePhoto}>
              Photos - add up to three photos, the first is the main photo
            </p>
            <p className={styles.availableDatesLetVendors}>
              Available dates - let vendors know which days the space is
              available on. Alternatively, click ‘Space is not time-limited’ if
              the space is generally always available.
            </p>
            <input
              className={styles.rectangleInput}
              type="text"
              placeholder="0/40"
              maxLength={40}
              minLength={0}
            />
            
           

            <p className={styles.clickAndDragToHighlightMu}>
              Click and drag to highlight multiple days at once, click and drag
              over highlighted days to deselect them
            </p>
            <div className={styles.datePickerDiv}>
              <DatePicker
                label="Chhose Date"
                value={datePickerDateTimePickerValue}
                onChange={(newValue) => {
                  setDatePickerDateTimePickerValue(newValue);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    color="warning"
                    variant="standard"
                    size="medium"
                    renderInput={{ placeholder: "01/01/2022" }}
                    helperText=""
                    autoFocus
                  />
                )}
              />
            </div>
            <img className={styles.errorIcon} alt="" src="../error6.svg" />
            <p className={styles.spaceIsNotTimeLimited}>
              Space is not time-limited
            </p>
            <Form.Check className={styles.checkBoxFormCheck} isInvalid />
            <Link href="/my-spaces">
              <a className={styles.closeA}>
                <img className={styles.closeIcon} alt="" src="../close.svg" />
              </a>
            </Link>

            <button className={styles.buttonForm}
            autoFocus >
                <b className={styles.labelText}>Save Space</b>
                </button>
            </form>
          </div>
          


          <div className={styles.frameDiv4}>
            <div className={styles.frameDiv5}>
              <Link href="/">
                <a
                  className={styles.shareAshelf}
                  onClick={onShareAshelfLinkClick}
                >
                  <span className={styles.shareAshelfTxtSpan}>
                    <span className={styles.shareSpan}>share</span>
                    <span className={styles.aSpan}>A</span>
                    <span className={styles.shelfSpan}>shelf</span>
                  </span>
                </a>
              </Link>
              <button className={styles.groupButton} onClick={openMenuDrawer}>
                <div className={styles.groupDiv}>
                  <div className={styles.inputChipDiv}>
                    <img
                      className={styles.userImagesUserImages}
                      alt=""
                      src="../user-imagesuser-images.svg"
                    />
                  </div>
                  <img className={styles.menuIcon} alt="" />
                  <img className={styles.menuIcon1} alt="" src="../menu.svg" />
                </div>
              </button>
              <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
              <Link href="/signup">
                <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                  Sign Up:
                </a>
              </Link>
            </div>
            <div className={styles.hostAccountDiv}>Host Account</div>
          </div>
        </div>
        {isMenuDrawerOpen && (
          <PortalDrawer
            overlayColor="rgba(113, 113, 113, 0.3)"
            placement="Right"
            onOutsideClick={closeMenuDrawer}
          >
            <MenuDrawer onClose={closeMenuDrawer} />
          </PortalDrawer>
        )}
      </>
    </LocalizationProvider>
  );
};

export default EditMySpaces;
