import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./my-spaces.module.css";

const MySpaces = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onButtonClick = useCallback(() => {
    router.push("/edit-my-spaces");
  }, [router]);

  const onAddButtonClick = useCallback(() => {
    router.push("/edit-my-spaces");
  }, [router]);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <div className={styles.mySpacesDiv}>
        <Link href="/host-account">
          <a className={styles.profileA}>Profile</a>
        </Link>
        <h1 className={styles.mySpacesForRent}>My Spaces for Rent</h1>
        <Link href="/host-collaborations">
          <a className={styles.collaborations}>Collaborations</a>
        </Link>
        <a className={styles.mySpacesForRent1} href="/">
          My Spaces for Rent
        </a>
        <Link href="/host-account-setting">
          <a className={styles.accountSettings}>Account Settings</a>
        </Link>
        <Link href="/host-messages">
          <a className={styles.messages}>Messages</a>
        </Link>
        <Link href="/host-payments">
          <a className={styles.payments}>Payments</a>
        </Link>
        <section className={styles.section1}>
          <h2 className={styles.currentSpacesAvailable}>
            Current Spaces Available:
          </h2>
          <div className={styles.lineDiv} />
          <div className={styles.frameDiv}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-151@2x.png"
            />
            <b className={styles.shelfSpaceB}>Shelf Space</b>
            <button className={styles.button} onClick={onButtonClick}>
              <div className={styles.labelTextDiv}>Edit</div>
            </button>
          </div>
          <div className={styles.frameDiv1}>
            <button className={styles.addButton} onClick={onAddButtonClick}>
              <img className={styles.addIcon} alt="" src="../add.svg" />
            </button>
          </div>
        </section>
        <div className={styles.lineDiv1} />
        <div className={styles.lineDiv2} />
        <footer className={styles.groupFooter}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv3} />
        </footer>
        <div className={styles.frameDiv2}>
          <div className={styles.frameDiv3}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv1}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <div className={styles.hostAccountDiv}>Host Account</div>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default MySpaces;
